package controller

import (
	"github.com/gofiber/fiber/v2"
	"net/http"
	"todo-backend/dto"
	"todo-backend/service"
)


type TodoController struct {
	todoService service.TodoService
}

func New(todoService service.TodoService) TodoController {
	controller := TodoController{todoService: todoService}
	return controller
}

func (t *TodoController) GetAllTodos(c *fiber.Ctx) error {
	todos := t.todoService.GetAllTodos()

	response := dto.TodoResponse{}
	return c.JSON(response.FromEntity(todos))
}

func (t *TodoController) AddNewTodo(c *fiber.Ctx) error {
	c.Accepts("application/json")
	todo := new(dto.AddTodoRequest)

	if err := c.BodyParser(todo); err != nil {
		return err
	}
	err := t.todoService.AddTodo(todo.ToEntity())

	if err == nil {
		return c.SendStatus(http.StatusCreated)
	}else {
		return c.SendStatus(http.StatusBadRequest)
	}
}

