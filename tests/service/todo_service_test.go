package service

import (
	"github.com/google/uuid"
	"testing"
	"todo-backend/config"
	repo "todo-backend/repository"
	"todo-backend/service"
	"todo-backend/tests/mock"
)

var todoService service.TodoService

func init() {
	crateServer()
}

var todo = &repo.TodoEntity{
	Id:    uuid.New().String(),
	Title: "buy some milk",
}

func Test_when_getAllTodos_expect_returnAllTodos(t *testing.T) {
	todos := todoService.GetAllTodos()

	if len(todos) != 1 || todos[0].Title != "buy some milk" {
		t.Error()
	}
}

func Test_when_addTodo_expect_noError(t *testing.T) {
	err := todoService.AddTodo(*todo)
	if err != nil {
		t.Error()
	}
}

func crateServer() {
	config.LoadConfig()

	var todoArray []*repo.TodoEntity
	todoArray = append(todoArray, todo)
	todoService = service.New(mock.CreateMock(todoArray))
}