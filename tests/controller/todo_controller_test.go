package controller

import (
	"github.com/gofiber/fiber/v2"
	"github.com/steinfletcher/apitest"
	"io"
	"net/http"
	"testing"
	"todo-backend/config"
	handlers "todo-backend/controller"
	"todo-backend/service"
	"todo-backend/tests/mock"
)

var app *fiber.App
func init() {
	config.LoadConfig()

	todoService := service.New(mock.CreateMock(nil))
	app = config.Handlers(handlers.New(todoService))
}

func Test_when_requestAllTodo_expect_ok(t *testing.T) {
	apitest.Handler(FiberToHandlerFunc(app)).
		Get("/todos").
		Expect(t).
		Status(http.StatusOK).
		End()
}

func Test_when_requestAddTodo_expect_created(t *testing.T) {
	json := "{ \"title\" : \"buy some milk\" }"
	apitest.Handler(FiberToHandlerFunc(app)).
		Put("/todos").
		Body(json).
		ContentType("application/json").
		Expect(t).
		Status(http.StatusCreated).
		End()
}

func FiberToHandlerFunc(app *fiber.App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		resp, err := app.Test(r)
		if err != nil {
			panic(err)
		}

		for k, vv := range resp.Header {
			for _, v := range vv {
				w.Header().Add(k, v)
			}
		}
		w.WriteHeader(resp.StatusCode)

		if _, err := io.Copy(w, resp.Body); err != nil {
			panic(err)
		}
	}
}