package mock

import (
	repo "todo-backend/repository"
)

type Repository struct {
	findReturnValue []*repo.TodoEntity
}

func (r *Repository) Close() {
	println("Close Repository")
}

func (r *Repository) Find() ([]*repo.TodoEntity, error) {
	return r.findReturnValue, nil
}

func (r *Repository) Create(todo *repo.TodoEntity) error {
	return nil
}

func CreateMock(returnValue []*repo.TodoEntity) *Repository {
	mockRepo := &Repository{}
	mockRepo.findReturnValue = returnValue
	return mockRepo
}