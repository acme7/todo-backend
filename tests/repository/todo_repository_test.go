package repository

import (
	"database/sql"
	"log"
	"testing"
	"todo-backend/repository/postgres"

	r "todo-backend/repository"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

var todo = &r.TodoEntity{
	Id:    uuid.New().String(),
	Title: "test",
}

func NewMock() (*sql.DB, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	return db, mock
}

func Test_when_requestFind_expect_returnAllTodos(t *testing.T) {
	db, mock := NewMock()
	repo := &postgres.Repository{Db: db}
	defer func() {
		repo.Close()
	}()

	query := "SELECT id,title FROM todos.todo"

	rows := sqlmock.NewRows([]string{"id", "title"}).
		AddRow(todo.Id, todo.Title)

	mock.ExpectQuery(query).WillReturnRows(rows)

	todos, err := repo.Find()
	assert.NotEmpty(t, todos)
	assert.NoError(t, err)
	assert.Len(t, todos, 1)
}

func Test_when_insertNewTodo_expect_noError(t *testing.T) {
	db, mock := NewMock()
	repo := &postgres.Repository{Db: db}
	defer func() {
		repo.Close()
	}()

	query := "INSERT INTO todos.todo\\(id, title\\) VALUES \\(\\$1, \\$2\\)"

	prep := mock.ExpectPrepare(query)
	prep.ExpectExec().WithArgs(todo.Id, todo.Title).
		WillReturnResult(sqlmock.NewResult(0, 1))

	err := repo.Create(todo)
	assert.NoError(t, err)
}

func Test_when_insertNewTodo_expect_error(t *testing.T) {
	db, mock := NewMock()
	repo := &postgres.Repository{Db: db}
	defer func() {
		repo.Close()
	}()

	query := "INSERT INTO todos.todo\\(id, title\\) VALUES \\(\\$1, \\$2\\)"

	prep := mock.ExpectPrepare(query)
	prep.ExpectExec().WithArgs(todo.Id, todo.Title).
		WillReturnResult(sqlmock.NewResult(0, 0))

	err := repo.Create(todo)
	assert.Error(t, err)
}
