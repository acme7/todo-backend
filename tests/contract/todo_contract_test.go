package contract

import (
	"github.com/google/uuid"
	"github.com/pact-foundation/pact-go/dsl"
	"github.com/pact-foundation/pact-go/types"
	"github.com/spf13/viper"
	"log"
	"strconv"
	"testing"
	"todo-backend/config"
	handlers "todo-backend/controller"
	repo "todo-backend/repository"
	"todo-backend/service"
	"todo-backend/tests/mock"
)

var (
	pactUrl string
	brokerToken string
	appUrl string
	port string
)

func init() {
	config.LoadConfig()

	pactUrl = viper.Get("pact.pactUrl").(string)
	port =  strconv.Itoa(viper.Get("server.port").(int))
	brokerToken = viper.Get("pact.brokerToken").(string)
	appUrl = viper.Get("pact.appUrl").(string)
}

var todo = &repo.TodoEntity{
	Id:    uuid.New().String(),
	Title: "buy some milk",
}

func TestPactContract(t *testing.T) {
	pact := &dsl.Pact{
		Provider: "TodoWebProvider",
	}

	config.LoadConfig()

	var todoArray []*repo.TodoEntity
	todoArray = append(todoArray, todo)
	todoService := service.New(mock.CreateMock(todoArray))

	app := config.Handlers(handlers.New(todoService))
	go func() {
		log.Fatal(app.Listen(":" + port))
	}()

	_,err := pact.VerifyProvider(t, types.VerifyRequest{
		ProviderBaseURL:        appUrl + ":" + port,
		PactURLs:               []string{pactUrl},
		BrokerToken:            brokerToken,
		PublishVerificationResults: true,
		ProviderVersion:            "1.0.0",
	})

	if err != nil {
		println(err)
		t.Error(err)
	}
}
