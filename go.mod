module todo-backend

go 1.16

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/gofiber/fiber/v2 v2.8.0
	github.com/google/uuid v1.0.0
	github.com/lib/pq v1.3.0
	github.com/pact-foundation/pact-go v1.5.2
	github.com/spf13/viper v1.7.1
	github.com/steinfletcher/apitest v1.5.5
	github.com/stretchr/testify v1.7.0
)
