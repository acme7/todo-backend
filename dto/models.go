package dto

import (
	"github.com/google/uuid"
	"todo-backend/repository"
)

type TodoResponse struct {
	Id string `json:"id"`
	Title string `json:"title"`
}

func (o *TodoResponse) FromEntity(todoArray []*repository.TodoEntity) []*TodoResponse {
	todos := make([]*TodoResponse, 0)
	for _, todo := range todoArray {
		t := TodoResponse{
			Id:    todo.Id,
			Title: todo.Title,
		}

		todos = append(todos, &t)
	}

	return todos
}


type AddTodoRequest struct {
	Title string `json:"title"`
}

func (o *AddTodoRequest) ToEntity() repository.TodoEntity {
	return repository.TodoEntity{
		Id: uuid.New().String(),
		Title: o.Title,
	}
}


