CREATE SCHEMA todos;

CREATE TABLE todos.todo (
    id varchar(255) NOT NULL,
    title varchar(255) NOT NULL
);