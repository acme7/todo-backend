package main

import (
	"todo-backend/config"
)

func main() {
	config.StartServer()
}
