package config

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/spf13/viper"
	"log"
	"strconv"
	handlers "todo-backend/controller"
	"todo-backend/repository/postgres"
	"todo-backend/service"
)

func Handlers(controller handlers.TodoController) *fiber.App {
	app := fiber.New()
	app.Use(cors.New())

	app.Get("/todos", func(ctx *fiber.Ctx) error {
		return controller.GetAllTodos(ctx)
	})

	app.Put("/todos", func(ctx *fiber.Ctx) error {
		return controller.AddNewTodo(ctx)
	})

	return app
}

func StartServer()  {
	LoadConfig()

	uri := viper.Get("database.connectionUri")
	todosRepository, err := postgres.NewRepository("postgres", uri.(string), 1, 10)

	if err != nil {
		panic(err)
	}

	todoService := service.New(todosRepository)
	app := Handlers(handlers.New(todoService))

	port :=  strconv.Itoa(viper.Get("server.port").(int))
	log.Fatal(app.Listen(":" + port))
}