# Todo Backend Application

Simple Todo backend application.

## Tech Stack

- GoLang
- PostgresSQL
- Fiber
- Viper
- Pact Go
- Apitest
- Testify

## API Architecture

* Development application works here `http://167.172.12.177:3000`
* Production application works here `http://164.90.255.8:3000`


* The Router requests are:

    - **GET**: `/todos` Get all todos.
    - **PUT**: `/todos` Add new todo.

## Testing

  ```
  go test -v ./...
  ```

## Running on local

```
 - docker-compose up -d 
 - go get
 - go build
 - profile=local go run todo-backend
```




