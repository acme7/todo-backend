package repository

type TodoRepository interface {
	Close()
	Find() ([]*TodoEntity, error)
	Create(todo *TodoEntity) error
}

type TodoEntity struct {
	Id string
	Title string
}