package postgres

import (
	"context"
	"database/sql"
	"errors"
	_ "github.com/lib/pq"
	"time"
	repo "todo-backend/repository"
)

type Repository struct {
	Db *sql.DB
}

func NewRepository(dialect, dsn string, idleConn, maxConn int) (repo.TodoRepository, error) {
	db, err := sql.Open(dialect, dsn)
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	db.SetMaxIdleConns(idleConn)
	db.SetMaxOpenConns(maxConn)

	return &Repository{db}, nil
}

func (r *Repository) Close() {
	err := r.Db.Close()
	if err != nil {
		return 
	}
}

func (r *Repository) Find() ([]*repo.TodoEntity, error) {
	todos := make([]*repo.TodoEntity, 0)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	query := "SELECT id,title FROM todos.todo"
	rows, err := r.Db.QueryContext(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		todo := new(repo.TodoEntity)
		err = rows.Scan(
			&todo.Id,
			&todo.Title,
		)

		if err != nil {
			return nil, err
		}
		todos = append(todos, todo)
	}

	return todos, nil
}

func (r *Repository) Create(todo *repo.TodoEntity) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	query := "INSERT INTO todos.todo(id, title) VALUES ($1, $2)"
	stmt, err := r.Db.PrepareContext(ctx, query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	execContext, err := stmt.ExecContext(ctx, todo.Id, todo.Title)
	if err != nil {
		return err
	}

	affected, err := execContext.RowsAffected()
	if err != nil {
		return err
	}

	if affected == 0 {
		return errors.New("no effected rows")
	}

	return err
}

