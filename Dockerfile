FROM golang:1.16

COPY . /app

WORKDIR /app/

RUN go mod download
RUN go build -o main

CMD ["./main"]