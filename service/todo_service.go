package service

import (
	repo "todo-backend/repository"
)

type TodoService struct {
	todoRepo repo.TodoRepository
}

func New(repo repo.TodoRepository) TodoService {
	return TodoService{todoRepo: repo}
}

func (t *TodoService) GetAllTodos() []*repo.TodoEntity {
	todos, _ := t.todoRepo.Find()
	return todos
}

func (t *TodoService) AddTodo(todo repo.TodoEntity) error {
	err := t.todoRepo.Create(&repo.TodoEntity{
		Id: todo.Id,
		Title: todo.Title,
	})
	return err
}